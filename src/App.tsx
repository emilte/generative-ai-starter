import {
  ChatContainer,
  MainContainer,
  Message,
  MessageInput,
  MessageList,
  MessageProps,
} from "@chatscope/chat-ui-kit-react";
import "@chatscope/chat-ui-kit-styles/dist/default/styles.min.css";
import { FormEvent, useState } from "react";
import "./App.css";
import {
  formatAssistantImageMessage,
  formatAssistantMessage,
  formatUserMessage,
} from "./helpers";
import { askDALLE2, askGPT3, handleFunctionCall } from "./openai";

type ResponseMode = "chat" | "image";

function App() {
  const [input, setInput] = useState("");
  const [messages, setMessages] = useState<MessageProps[]>([]);

  // Set to "chat" or "image" to change response type
  const responseMode: ResponseMode = "chat";

  async function handleSend() {
    // Add user input to chat view
    const userMessage = formatUserMessage(input);
    setMessages((prev) => [...prev, userMessage]);

    // Check current response mode
    if (responseMode == "chat") {
      // For chat mode, use GPT-3.5-turbo
      const response = await askGPT3([...messages, userMessage]);
      if (response.function_call) {
        const functionCallResult = await handleFunctionCall(
          response.function_call
        );
        // Add the functionCallResult to the chat!
        setMessages((prev) => [
          ...prev,
          formatAssistantImageMessage(functionCallResult, input),
        ]);
      }
      if (response.content) {
        // There might be text content in addition to a function_call
        const answer = response.content;
        setMessages((prev) => [...prev, formatAssistantMessage(answer)]);
      }
    } else if (responseMode == "image") {
      // For image mode, use DALL-E 2
      const url = await askDALLE2(input);
      setMessages((prev) => [...prev, formatAssistantImageMessage(url, input)]);
    }
  }

  const handleInput = (e: FormEvent<HTMLDivElement>) => {
    e.preventDefault();
    setInput(e.currentTarget.innerText);
  };

  return (
    <div style={{ position: "relative", width: "800px", height: "100vh" }}>
      <MainContainer>
        <ChatContainer>
          <MessageList>
            {messages.map((messageProps, i) => (
              <Message key={i} {...messageProps} />
            ))}
          </MessageList>
          <MessageInput
            placeholder="Type message here"
            autoFocus
            attachButton={false}
            onSend={handleSend}
            onInput={handleInput}
          />
        </ChatContainer>
      </MainContainer>
    </div>
  );
}

export default App;
