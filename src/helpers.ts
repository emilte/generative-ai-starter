import { MessageProps } from "@chatscope/chat-ui-kit-react";
import {
  ChatCompletionRequestMessage,
  ChatCompletionRequestMessageRoleEnum,
} from "openai";

export function messagePropsToGPTMessage(
  messageProps: MessageProps
): ChatCompletionRequestMessage {
  return {
    role:
      messageProps?.model?.sender &&
      ["assistant", "user", "system"].includes(messageProps.model.sender)
        ? (messageProps.model.sender as ChatCompletionRequestMessageRoleEnum) // in this case, it's a valid role
        : "assistant",
    content: messageProps.model?.message ?? "",
  };
}

export function formatUserMessage(text: string): MessageProps {
  return {
    model: {
      message: text,
      sender: "user",
      direction: "outgoing",
      position: "normal",
    },
  };
}

export function formatAssistantMessage(text: string): MessageProps {
  return {
    model: {
      message: text,
      sender: "assistant",
      direction: "incoming",
      position: "normal",
    },
  };
}

export function formatAssistantImageMessage(
  src: string,
  alt: string
): MessageProps {
  return {
    // Add "image" type to render "payload" as an image
    type: "image",
    model: {
      sender: "assistant",
      direction: "incoming",
      position: "normal",
      payload: {
        src,
        alt,
        width: "512px",
      },
    },
  };
}
