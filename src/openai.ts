import { MessageProps } from "@chatscope/chat-ui-kit-react";
import {
  ChatCompletionRequestMessage,
  ChatCompletionRequestMessageFunctionCall,
  ChatCompletionResponseMessage,
  Configuration,
  OpenAIApi,
} from "openai";
import { messagePropsToGPTMessage } from "./helpers";

const configuration = new Configuration({
  apiKey: import.meta.env.VITE_OPENAI_API_KEY,
});
delete configuration.baseOptions.headers["User-Agent"];
const openai = new OpenAIApi(configuration);

// const SYSTEM_PROMPT = "You are a helpful AI assistant.";
// const SYSTEM_PROMPT = "You are a teenager who giggles a lot.";
// const SYSTEM_PROMPT = "You are a grumpy old man.";
// const SYSTEM_PROMPT = "You are a sarcastic best friend.";
// const SYSTEM_PROMPT = "You are an anxious nurse.";
// const SYSTEM_PROMPT = `You are a helpful AI assistant. Format your replies as JSON, like this: {"response": "..."}`;
const SYSTEM_PROMPT = `You are a nerdy teenager fanatically concerned with movies and TV-series. You relate everything to a series or a movie. You attempt to quote references to them for every request directed at you.`;

export const askGPT3 = async (
  messages: MessageProps[]
): Promise<ChatCompletionResponseMessage> => {
  // Convert chat format (MessageProps) to GPT format (ChatCompletionRequestMessage)
  const gptMessages: ChatCompletionRequestMessage[] = messages.map(
    messagePropsToGPTMessage
  );

  // Request a GPT chat completion
  const result = await openai.createChatCompletion({
    model: "gpt-3.5-turbo",
    messages: [
      {
        // One quirk with GPT-3.5-turbo is that it is not good at following "system" role prompts.
        role: "system",
        content: SYSTEM_PROMPT,
      },
      ...gptMessages,
    ],
    functions: [
      {
        name: "generateImage",
        description:
          "Generate an image based on the imageDescription parameter",
        parameters: {
          type: "object",
          properties: {
            imageDescription: {
              type: "string",
              description:
                "A declarative description of the image that will be generated",
            },
          },
          required: ["imageDescription"],
        },
      },
    ],
    function_call: "auto",
  });

  // Uncomment the line below to learn more about the API results:
  // console.log(result);

  if (!result.data.choices[0].message)
    throw new Error("Chat Completion generation failed.");

  // Return the first message's content (text)
  return result.data.choices[0].message;
};

export const askDALLE2 = async (prompt: string): Promise<string> => {
  // Generate an image, only "prompt" (text) is required.
  const result = await openai.createImage({
    prompt,
    size: "512x512",
    n: 1,
    response_format: "url",
  });

  // Uncomment the line below to learn more about the API results:
  // console.log(result);

  if (!result.data.data[0].url) throw new Error("Image generation failed.");

  // Return the first image's url (update if you make edits to "n" or "response_format" above)
  return result.data.data[0].url;
};

type GenerateImageArgs = {
  imageDescription: string;
};

export const handleFunctionCall = async (
  functionCall: ChatCompletionRequestMessageFunctionCall
): Promise<string> => {
  const availableFunctions = ["generateImage"];
  const functionName = functionCall.name;
  if (!functionName || !availableFunctions.includes(functionName))
    throw new Error("Invalid functionName");

  const functionArgs = functionCall.arguments;
  if (!functionArgs) throw new Error("Missing required arguments.");

  if (functionName == "generateImage") {
    const parsedArgs = JSON.parse(functionArgs) as GenerateImageArgs;
    const generatedImageUrl = await askDALLE2(parsedArgs.imageDescription);
    return generatedImageUrl;
    // Try implementing other functions to call!
    // } else if (functionName == "...") {
  } else throw new Error(`Unhandled function: ${functionName}`);
};
